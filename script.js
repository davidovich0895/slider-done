//Создаю фнкцию конструктор для создания слайдов
var Konstruktor = function(textSlide,image){
	this.textSlide = textSlide;
	this.image = image;
	this.oneSlide();
};

Konstruktor.prototype.oneSlide = function(){
	var slider = document.querySelector('.slider');
	var slide = document.createElement('div');
	slide.classList.add('slide');
	slide.style.background = 'linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url(' + this.image + ')';
	slide.style.backgroundPosition = 'center';
	slider.appendChild(slide);
	var slideName = document.createElement('h1');
	slideName.textContent = this.textSlide;
	slide.appendChild(slideName);
};

var slide1 = new Konstruktor('Я люблю мандарины', '1.jpg');
var slide2 = new Konstruktor('Я люблю апельсины', '2.jpg');
var slide3 = new Konstruktor('Я люблю хурму', '3.jpg');
var slide4 = new Konstruktor('Я люблю виноград', '4.jpg');

//---------------------------------------------------------------------

// Задаем номер стартовой картинки, глобальные переменные
var slideIndex = 1;
var slides = document.querySelectorAll('.slide');
var dots = document.querySelector('.slider-dots');
var buttonPrev = document.querySelector('.prev');
buttonPrev.onclick = minusSlide;
var buttonNext = document.querySelector('.next');
buttonNext.onclick = plusSlide;
// Вызыв основных функций
createDots();
showSlides();

//Создаю точки, число которых будет соответсвовать числу картинкок
function createDots(){
	for(let j = 0; j < slides.length; j++){
		var dot = document.createElement('span');
		dot.classList.add('slider-dots_item');
		dot.onclick = function(){
			showSlides(slideIndex= j+1);
		};
		dots.appendChild(dot);
	}
};

//Скрываем все картинки кроме первой
function showSlides(){
	if(slideIndex > slides.length){
		slideIndex = 1;
	} else if(slideIndex < 1){
		slideIndex = slides.length;
	}
	var dott = dots.children;
	for(let i = 0; i < slides.length; i++){
		slides[i].style.display = 'none';
		dott[i].classList.remove('active');
	}
	slides[slideIndex-1].style.display = 'block';
	dott[slideIndex-1].classList.add('active');
};

//Предыдущий слайд
function minusSlide(){
	showSlides(--slideIndex);
}

//Следующий слайд
function plusSlide(){
	showSlides(++slideIndex);
}
